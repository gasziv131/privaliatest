//
//  PrivaliaAppTests.swift
//  PrivaliaAppTests
//
//  Created by Gaston Zitelli on 2017-01-11.
//  Copyright © 2017 Gaston Zitelli. All rights reserved.
//

import XCTest
@testable import PrivaliaApp

class PrivaliaAppTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSubstractMonthAndDay() {
       let dateToFormat = DateManager.substractMonthAndDay(date: "2017-03-12")
        XCTAssertEqual(dateToFormat, "2017", "dateToFormat is not equals to date expected")
    }
    
    
}
