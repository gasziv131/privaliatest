//
//  MovieTableViewCell.swift
//  PrivaliaApp
//
//  Created by Gaston Zitelli on 2017-01-11.
//  Copyright © 2017 Gaston Zitelli. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var txtOverView: UILabel!

    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
