//
//  MovieEntity.swift
//  PrivaliaApp
//
//  Created by Gaston Zitelli on 2017-01-11.
//  Copyright © 2017 Gaston Zitelli. All rights reserved.
//

import Foundation

import Foundation

class MovieEntity:NSObject {
    var original_title: String
    var release_date: String
    var overview: String
    var poster_path: String
    
    init(original_title: String, release_date: String, overview: String, poster_path: String) {
        self.original_title = original_title
        self.release_date = release_date
        self.overview = overview
        self.poster_path = poster_path
    }
    
    
}
