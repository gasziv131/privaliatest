//
//  DateManager.swift
//  PrivaliaApp
//
//  Created by Gaston Zitelli on 2017-01-16.
//  Copyright © 2017 Gaston Zitelli. All rights reserved.
//

import Foundation

class DateManager{
    static func substractMonthAndDay(date:String) -> String{
        let index = date.index(date.startIndex, offsetBy: 4)
        return date.substring(to: index)
    }
}
