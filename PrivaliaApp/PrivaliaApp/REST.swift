//
//  REST.swift
//  PrivaliaApp
//
//  Created by Gaston Zitelli on 2017-01-11.
//  Copyright © 2017 Gaston Zitelli. All rights reserved.
//

import Foundation

class REST{
    
    
   static func getMoviesBy(currentPage: NSInteger, callback: @escaping (([MovieEntity]) -> ())){
              let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "https://api.themoviedb.org/4/list/1?page=\(currentPage)&api_key=93aea0c77bc168d8bbce3918cefefa45")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                
                print(error!.localizedDescription)
                
            } else {
              
                var movieArray = [MovieEntity]()
                
                do {
                    
                    if let data = data,
                        let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                        let movies = json["results"] as? [[String: Any]] {

                        
                        for movieItem in movies {
                            
                            guard let original_title = movieItem["original_title"] as? String,
                                    let release_date = movieItem["release_date"] as? String,
                                    let overview = movieItem["overview"] as? String,
                                    let poster_path = movieItem["poster_path"] as? String else{
                                    
                                    return
                            }
                            
                            let movieEntity =  MovieEntity.init(original_title: original_title, release_date: release_date, overview: overview, poster_path: poster_path)
                            
                            movieArray.append(movieEntity)
                        }
                        
                        DispatchQueue.main.async(){
                            callback(movieArray)
                        }
                        
                    }
                    
                 } catch {
                    print("error in JSONSerialization")
                    
                }
                
                
            }
            
        })
        task.resume()
    }
    
   
}

