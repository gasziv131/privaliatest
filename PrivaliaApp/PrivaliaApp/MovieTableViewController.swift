//
//  MovieTableViewController.swift
//  PrivaliaApp
//
//  Created by Gaston Zitelli on 2017-01-11.
//  Copyright © 2017 Gaston Zitelli. All rights reserved.
//

import UIKit

class MovieTableViewController: UITableViewController, UISearchResultsUpdating {
    
    var movieArray = [MovieEntity]()
    var filteredMovies = [MovieEntity]()
    var searchController: UISearchController!
    var currentPage = 1
    let activityIndicator = UIActivityIndicatorView()
    let baseUrl = "https://image.tmdb.org/t/p/w500"
    var canLoad = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupActivityIndicator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadMovieData()
        self.setupSearchBar()
        self.setupInfiniteScroll()
        self.setupTableViewUI()
    }
    
    private func setupTableViewUI(){
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 160
    }
    
    private func setupActivityIndicator(){
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.white
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating();
    }
    
    private func loadMovieData(){
        
        REST.getMoviesBy(currentPage: self.currentPage) { MovieEntityArray in
            
            if MovieEntityArray.count == 0 {
                self.hideInfinitiScroll()
            }else{
                self.fillMovieArraysAndRefreshTable(movieEntityArray: MovieEntityArray)
            }
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.canLoad = true
            }
            
        }
    }
    
    private func fillMovieArraysAndRefreshTable(movieEntityArray: [MovieEntity]) -> Void{
        for movieItem in movieEntityArray{
            self.movieArray.append(movieItem)
        }
        self.tableView.reloadData()
        
    }
    
    private func setupInfiniteScroll(){
        tableView.infiniteScrollIndicatorStyle = .white
        tableView.addInfiniteScroll { (tableView) -> Void in
            
            DispatchQueue.main.async {tableView.finishInfiniteScroll()}
            
        }
    }
    
    private func hideInfinitiScroll(){
        self.tableView.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return false
        }
    }
    
    private func setupSearchBar(){
        
        self.searchController = ({
            searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.dimsBackgroundDuringPresentation = false
            searchController.searchBar.placeholder = "Search for movie"
            searchController.searchBar.sizeToFit()
            searchController.searchBar.barStyle = UIBarStyle.black
            searchController.searchBar.tintColor = UIColor.black
            
            
            self.tableView.tableHeaderView = searchController.searchBar
            
            return searchController
        })()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchController.isActive {
            return self.filteredMovies.count
        }
        else {
            return self.movieArray.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
        
        if self.searchController.isActive {
            let movie = filteredMovies[indexPath.row]
            
            cell.lblTitle?.text = movie.original_title
            cell.lblYear?.text = movie.release_date
            cell.txtOverView?.text = movie.overview
            cell.imgPoster?.downloadedFrom(link: baseUrl + movie.poster_path)
            
        }else{
            let movie = movieArray[indexPath.row]
            cell.lblTitle?.text = movie.original_title
            cell.lblYear?.text = movie.release_date
            cell.txtOverView?.text = movie.overview
            cell.imgPoster?.downloadedFrom(link: baseUrl + movie.poster_path)
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == movieArray.count - 1 && canLoad {
            currentPage += 1
            self.loadMovieData()
            canLoad = false
        }
    }
    
    func updateSearchResults(for searchController: UISearchController){
        filteredMovies.removeAll(keepingCapacity: false)
        
        let searchPredicate = NSPredicate(format: "original_title contains[c] %@", searchController.searchBar.text!)
        filteredMovies = (movieArray as NSArray).filtered(using: searchPredicate) as! [MovieEntity]
        
        
        self.tableView.reloadData()
    }
    
}
